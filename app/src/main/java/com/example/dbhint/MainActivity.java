package com.example.dbhint;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс основной Activity  приложения
 *
 * @author Komissarova M., 16IT18k
 * @author Chelnokov E., 16IT18k
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Метод, создающий или перезапускающий Activity
     * @param savedInstanceState Текущее состояние Activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        String[] itemArray = getResources().getStringArray(R.array.array_of_menu_items);

        List<ItemFragment> itemList = new ArrayList<>();

        for (String s : itemArray) {
            itemList.add(new ItemFragment(s));
        }

        RecyclerView.Adapter mAdapter = new MyAdapter(itemList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

    }

    /**
     * Метод, отвечающий за отображение меню в Activity
     * Заполняет объект меню, используя файл ресурсов
     * @param menu объект меню в Activity
     * @return отображаемость меню в Activity
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    /**
     * Осуществляет переход на другую Activity при выборе пункта меню
     * @param item выбранный пункт меню
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("itemId",item.getItemId());
        startActivity(intent);
        return true;
    }

    public void onClick(View view) {
        TextView textView = (TextView)view;
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("filename", textView.getText());
        startActivity(intent);
    }

}

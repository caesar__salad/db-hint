package com.example.dbhint;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        String filename;
        int itemId = getIntent().getIntExtra("itemId", 0);
        if (itemId == 0){
            filename = getIntent().getStringExtra("filename");
            viewFile(filename);
        }else {
            itemSelection(itemId);
        }
    }

   private void itemSelection(int itemId) {
        String fileName = "";
        switch (itemId) {
            case R.id.DataType:
                fileName = "DataType";
                break;
            case R.id.DDL:
                fileName = "DDL";
                break;
            case R.id.DML:
                fileName = "DML";
                break;
            case R.id.DCL:
                fileName = "DCL";
                break;
            case R.id.TCL:
                fileName = "TCL";
                break;
        }

        viewFile(fileName);
    }

    private void viewFile(String fileName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            StringBuilder stringBuilder = getStringFromAssetFile(fileName);
            String content = stringBuilder.toString();
            TextView textView = findViewById(R.id.textView);
            textView.setText(Html.fromHtml(content));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected StringBuilder getStringFromAssetFile(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open(fileName)))) {
            String string;
            while ((string = reader.readLine()) != null) {
                stringBuilder.append(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

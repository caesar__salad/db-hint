package com.example.dbhint;

import androidx.fragment.app.Fragment;

public class ItemFragment extends Fragment {
    private String item;

    public String getItem() {
        return item;
    }

    public ItemFragment(String item) {
        this.item = item;
    }
}

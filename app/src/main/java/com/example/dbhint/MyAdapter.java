package com.example.dbhint;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private List<ItemFragment> items;

    MyAdapter(List<ItemFragment> itemList){
        this.items = itemList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.list_item);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.item.setText(items.get(position).getItem());
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


}
